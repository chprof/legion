var map, infoWindow, markers = [], image, imageActive, shape, directionInfoWindow, markerWindow;
var cities = {
	all: {
		center: { lat: 41.3021592, lng: 60.0851922 },
		zoom: 6,
	},
	tashkent: {
		center: { lat: 41.2827052, lng: 69.2093271 },
		zoom: 11,
	},
	samarqand: {
		center: { lat: 39.6407995, lng: 66.8978474 },
		zoom: 12,
	},
	fergana: {
		center: { lat: 40.3798349, lng: 71.7554421 },
		zoom: 13,
	},
	namangan: {
		center: { lat: 40.970576, lng: 71.5745829 },
		zoom: 12,
	},
	andijan: {
		center: { lat: 40.7794732, lng: 72.2496058 },
		zoom: 12,
	},
	nukus: {
		center: { lat: 42.4714808, lng: 59.5337901 },
		zoom: 12,
	},
	bukhara: {
		center: { lat: 39.777597, lng: 64.3877787 },
		zoom: 13,
	},
	qarshi: {
		center: { lat: 38.8583673, lng: 65.7217579 },
		zoom: 12,
	},
};
var shops = [
	{ 
		storeName: '#1',
		center: { lat: 41.2849176, lng: 69.2052416 },
		phone: '998991112233',
		address: 'Lorem ipsum dolor sit, amet conse',
		img: 'img/store1.jpg',
		schedule: 'пн-пт, 9:00-20:00'
	},
	{
		storeName: '#2',
		center: { lat: 41.3118564, lng: 69.2853588 },
		phone: '998991112233',
		address: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Molestiae, quibusdam!',
		img: 'img/product-img-1.jpg',
		schedule: 'пн-пт, 9:00-20:00'
	},
	{
		storeName: '#3',
		center: { lat: 39.6407995, lng: 66.8978474 },
		phone: '998991112233',
		address: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Id provident omnis quas necessitatibus soluta odio.',
		img: 'img/product-img-2.jpg',
		schedule: 'пн-пт, 9:00-20:00'
	},
	{
		storeName: '#4',
		center: { lat: 40.3798349, lng: 71.7554421 },
		phone: '998991112233',
		address: 'Lorem ipsum dolor sit amet.',
		img: 'img/product-img-3.jpg',
		schedule: 'пн-пт, 9:00-20:00'
	},
	{
		storeName: '#5',
		center: { lat: 40.970576, lng: 71.5745829 },
		phone: '998991112233',
		address: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Molestiae, quibusdam!',
		img: 'img/product-img-4.jpg',
		schedule: 'пн-пт, 9:00-20:00'
	},
	{
		storeName: '#6',
		center: { lat: 40.7794732, lng: 72.2496058 },
		phone: '998991112233',
		address: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Id provident omnis quas necessitatibus soluta odio.',
		img: 'img/store1.jpg',
		schedule: 'пн-пт, 9:00-20:00'
	},
	{
		storeName: '#7',
		center: { lat: 42.47768481206322, lng: 59.6045145814836 },
		phone: '998991112233',
		address: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Molestiae, quibusdam!',
		img: 'img/product-img-1.jpg',
		schedule: 'пн-пт, 9:00-20:00'
	},
	{
		storeName: '#8',
		center: { lat: 39.777597, lng: 64.3877787 },
		phone: '998991112233',
		address: 'Lorem ipsum dolor sit amet.',
		img: 'img/product-img-2.jpg',
		schedule: 'пн-пт, 9:00-20:00'
	},
	{
		storeName: '#9',
		center: { lat: 38.8583673, lng: 65.7217579 },
		phone: '998991112233',
		address: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Id provident omnis quas necessitatibus soluta odio.',
		img: 'img/product-img-3.jpg',
		schedule: 'пн-пт, 9:00-20:00'
	},
]
var directionsService, directionsRenderer;
function initMap() {
	directionsService = new google.maps.DirectionsService();
	directionsRenderer = new google.maps.DirectionsRenderer();
	map = new google.maps.Map(document.getElementById("map"), {
		zoom: cities["tashkent"].zoom,
		center: cities["tashkent"].center,
		scrollwheel: false,
		navigationControl: false,
		scaleControl: true,
		draggable: true,
		mapTypeControl: false,
		panControl: false,
		zoomControl: true,
		streetViewControl: false,
		fullscreenControl: false,
		styles: [
			{
					"featureType": "all",
					"elementType": "geometry.fill",
					"stylers": [
							{
									"weight": "2.00"
							}
					]
			},
			{
					"featureType": "all",
					"elementType": "geometry.stroke",
					"stylers": [
							{
									"color": "#9c9c9c"
							}
					]
			},
			{
					"featureType": "all",
					"elementType": "labels.text",
					"stylers": [
							{
									"visibility": "on"
							}
					]
			},
			{
					"featureType": "landscape",
					"elementType": "all",
					"stylers": [
							{
									"color": "#f2f2f2"
							}
					]
			},
			{
					"featureType": "landscape",
					"elementType": "geometry.fill",
					"stylers": [
							{
									"color": "#ffffff"
							}
					]
			},
			{
					"featureType": "landscape.man_made",
					"elementType": "geometry.fill",
					"stylers": [
							{
									"color": "#ffffff"
							}
					]
			},
			{
					"featureType": "poi",
					"elementType": "all",
					"stylers": [
							{
									"visibility": "off"
							}
					]
			},
			{
					"featureType": "road",
					"elementType": "all",
					"stylers": [
							{
									"saturation": -100
							},
							{
									"lightness": 45
							}
					]
			},
			{
					"featureType": "road",
					"elementType": "geometry.fill",
					"stylers": [
							{
									"color": "#eeeeee"
							}
					]
			},
			{
					"featureType": "road",
					"elementType": "labels.text.fill",
					"stylers": [
							{
									"color": "#7b7b7b"
							}
					]
			},
			{
					"featureType": "road",
					"elementType": "labels.text.stroke",
					"stylers": [
							{
									"color": "#ffffff"
							}
					]
			},
			{
					"featureType": "road.highway",
					"elementType": "all",
					"stylers": [
							{
									"visibility": "simplified"
							}
					]
			},
			{
					"featureType": "road.arterial",
					"elementType": "labels.icon",
					"stylers": [
							{
									"visibility": "off"
							}
					]
			},
			{
					"featureType": "transit",
					"elementType": "all",
					"stylers": [
							{
									"visibility": "off"
							}
					]
			},
			{
					"featureType": "water",
					"elementType": "all",
					"stylers": [
							{
									"color": "#46bcec"
							},
							{
									"visibility": "on"
							}
					]
			},
			{
					"featureType": "water",
					"elementType": "geometry.fill",
					"stylers": [
							{
									"color": "#c8d7d4"
							}
					]
			},
			{
					"featureType": "water",
					"elementType": "labels.text.fill",
					"stylers": [
							{
									"color": "#070707"
							}
					]
			},
			{
					"featureType": "water",
					"elementType": "labels.text.stroke",
					"stylers": [
							{
									"color": "#ffffff"
							}
					]
			}
		]
	});
	setMarkers(map);
	directionsRenderer.setMap(map);
	[].slice.call(document.querySelectorAll("[data-select-map]")).forEach(function(link, index, array) {
		link.addEventListener("click", function(e) {
			// directionsRenderer.setMap(null);
			var sortedLinks = array.filter(function(_link) {
				if(_link !== link) {
					return _link
				}
			});
			sortedLinks.forEach(function(_link) {
				_link.classList.remove('active')
			})
			setAutocompleteCountry(e, map, link)
		});
	});
};
var onChangeHandler = function (position, dest) {
	calculateAndDisplayRoute(directionsService, directionsRenderer, position, dest);
};
function calculateAndDisplayRoute(directionsService, directionsRenderer, position, dest) {
  directionsService.route(
    {
      origin: new google.maps.LatLng(position.lat, position.lng),
      destination: new google.maps.LatLng(dest.lat, dest.lng),
      travelMode: google.maps.TravelMode.DRIVING,
    },
    (response, status) => {
      if (status === "OK") {
        directionsRenderer.setDirections(response);
      } else {
        window.alert("Directions request failed due to " + status);
      }
    }
  );
};
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(
    browserHasGeolocation
      ? "Error: The Geolocation service failed."
      : "Error: Your browser doesn't support geolocation."
  );
  infoWindow.open(map);
}
function setAutocompleteCountry(e, map, link) {
	e.preventDefault();
	var city = link.getAttribute('href').slice(1),
			cityText = link.textContent;
	
	link.closest('.select').querySelector('.select__btn-txt').textContent = cityText;
	link.classList.add('active')
	infoWindowStatus('hide');
	markers.forEach(function(regularMarker, j) {
		regularMarker.setIcon(image);
	})
	map.setCenter(cities[city].center);
	map.setZoom(cities[city].zoom);
};
function setMarkers(map) {
	image = {
		url: "img/icons/loc-grey.svg",
		size: new google.maps.Size(26, 42),
		origin: new google.maps.Point(0, 0),
		anchor: new google.maps.Point(11, 42),
	};
	imageActive = {
		url: "img/icons/loc-red.svg",
		size: new google.maps.Size(26, 42),
		origin: new google.maps.Point(0, 0),
		anchor: new google.maps.Point(11, 42),
	};
	shape = {
		coords: [0, 0, 26, 0, 26, 42, 0, 42, 0, 0],
		type: "poly",
	};
	var marker;
	shops.forEach(function(shop, i) {
		marker = new google.maps.Marker({
			position: shop.center,
			map,
			icon: image,
			shape: shape,
			phone: shop.phone,
			address: shop.address
		});
		markers.push(marker);
		google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				markers.forEach(function(regularMarker, j) {
					regularMarker.setIcon(image);
					infoWindowStatus('hide', null)
				})
				this.setIcon(imageActive);
				infoWindowStatus('show', shop)
			}
		})(marker, i));
		google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
			return function() {
				// markers.forEach(function(regularMarker, j) {
				// 	regularMarker.setIcon(image);
				// })
				// this.setIcon(imageActive);
				var infoMarkerString = `<div class="marker-info">
																<strong class="marker-info__title">${shop.storeName}</strong>
																<div class="marker-info__address">${shop.address}</div>
															</div>`;
				markerWindow.setContent(infoMarkerString);
				markerWindow.open(map, marker, infoMarkerString);
			}
		})(marker, i))
		google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
			return function() {
				// this.setIcon(image);
				markerWindow.close();
			}
		})(marker, i))
	})
};
function infoWindowStatus(status, shop = null) {
	if(status && shop) {
		var storeNameTag = document.getElementById('store-info-name');
		var imgTag = document.getElementById('store-info-img');
		var addressTag = document.getElementById('store-info-address');
		var scheduleTag = document.getElementById('store-info-schedule');
		var phoneTag = document.getElementById('store-info-phone');
		var directionLink = document.getElementById('store-info-link');
		// console.log(Swiper);
		// console.log(elementScroller)

		storeNameTag.textContent = shop.storeName;
		imgTag.src = shop.img;
		addressTag.textContent = shop.address;
		scheduleTag.textContent = shop.schedule;
		phoneTag.textContent = shop.phone;
		phoneTag.setAttribute('href', `tel:+${shop.phone}`);
		directionLink.setAttribute('data-lat', shop.center.lat)
		directionLink.setAttribute('data-lng', shop.center.lng)

		infoWindow.closest('.popup').classList.add('active');
		if(window.innerWidth >= 992) {
			infoWindow.parentElement.classList.add('animated', 'back-in-right');
		}

		setTimeout(function() {
			infoWindow.parentElement.classList.remove('animated', 'back-in-right')
		}, 1000)
		// setTimeout(function() {
		// 	elementScroller.update()
		// }, 1200)
	} else {
		infoWindow.closest('.popup').classList.remove('active');
	}
}
document.addEventListener("DOMContentLoaded", () => {
	if (document.querySelector('#map')) {
		initMap();
		infoWindow = document.getElementById('store-info');
		directionInfoWindow = new google.maps.InfoWindow();
		markerWindow = new google.maps.InfoWindow();
		document.getElementById('card-store-close').addEventListener('click', function(e) {
			e.preventDefault();
			infoWindow.closest('.popup').classList.remove('active');
		})
		document.getElementById('store-info-link').addEventListener('click', function(e) {
			e.preventDefault();
			infoWindowStatus('hide', null)
			var newPosition;
			var dest = { lat: e.currentTarget.dataset.lat, lng: e.currentTarget.dataset.lng };
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(
					(position) => {
						
						const pos = {
							lat: position.coords.latitude,
							lng: position.coords.longitude,
						};
						newPosition = pos;
						// let new_window = window.open('https://maps.google.com/maps?saddr=' + pos.lat + ',' + pos.lng + '&daddr=' + dest.lat + ',' + dest.lng)
						let new_window = window.open('https://maps.google.com/maps?saddr=' + pos.lat + ',' + pos.lng + '&daddr=' + dest.lat + ',' + dest.lng)
						console.log(new_window);
						// onChangeHandler(newPosition, dest)

					},
					() => {
						handleLocationError(true, directionInfoWindow, map.getCenter());
					}
				);
			} else {
				handleLocationError(false, directionInfoWindow, map.getCenter());
			}
		});
		
	}
});