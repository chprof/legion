// libs
// =================

import { Swiper, Mousewheel, Scrollbar, Parallax, Lazy, Navigation } from 'swiper';
import Inputmask from "inputmask";
import MicroModal from "micromodal";
import svg4everybody from "svg4everybody";
Swiper.use([Mousewheel, Scrollbar, Parallax, Lazy, Navigation]);


// =================
// =================


// app
// ==================

import toggle from '../scripts/app/toggle';
import videoSettings from '../scripts/app/videoSettings';
import controlSound from '../scripts/app/controlSound';
import setPromoAndAgeLimitConfig from '../scripts/app/setPromoAndAgeLimitConfig';
import actionsAfterCallConfirmOrRepeatVideo from '../scripts/app/actionsAfterCallConfirmOrRepeatVideo';
import setPreviewsLogic from '../scripts/app/setPreviewsLogic';
import setAccordionLogic from '../scripts/app/setAccordionLogic';
import setSelectLogic from '../scripts/app/setSelectLogic';
import setFrontSideMobileLogic from '../scripts/app/setFrontSideMobileLogic';
import setFoundersLogic from '../scripts/app/setFoundersLogic';
import setProductLogic from '../scripts/app/setProductLogic';
import setInputMaskLogic from '../scripts/app/setInputMaskLogic';
import setCarouselLogic from '../scripts/app/setCarouselLogic';
import setFooterLogic from '../scripts/app/setFooterLogic';

// ==================
// ==================


// baseConfig
// ===================


const fullPageSelector = '[data-full-page]',
	fullPageScrollerSelector = '[data-full-page-scroller]',
	menuScrollerSelector = '[data-menu-scroller]',
	selectScrollerSelector = '[data-select-scroller]',
	carouselEmployeesSelector = '[data-carousel-employees]',
	elementScrollerSelector = '[data-element-scroller]',
	mapScrollerSelector = '[data-map-scroller]',
	carouselEmployeesNavNext = '[data-carousel-employees-next]',
	carouselEmployeesNavPrev = '[data-carousel-employees-prev]';

let fullPage,
	menuScroller,
	selectScroller,
	elementScroller,
	mapScroller,
	fullPageScroller,
	carouselEmployees,
	update,
	updateScroll,
	timer,
	preloader = document.querySelector('[data-preloader]');

if (!Object.keys(window.localStorage).length) {
	window.localStorage.setItem('legionVisited', 'false');
	window.localStorage.setItem('legionPromoSound', 'true');
	window.localStorage.setItem('legionPromoWatched', 'false');
}

const menuScrollerConfig = {
	direction: 'vertical',
	slidesPerView: 'auto',
	freeMode: true,
	mousewheel: {
		// sensitivity: 0.2
	},
	scrollbar: {
		el: '.swiper-scrollbar--menu',
		// hide: true,
		// draggable: true
	},
	on: {
		// afterInit(swiper) {
		// 	swiper.scrollbar.updateSize()
		// }
	}
};

const selectScrollerConfig = {
	direction: 'vertical',
	slidesPerView: 'auto',
	freeMode: true,
	mousewheel: {
		sensitivity: 0.2
	},
	nested: true,
	scrollbar: {
		el: '.swiper-scrollbar--select',
		hide: false,
		draggable: true
	},
	on: {
		update(swiper) {
			swiper.scrollbar.updateSize()
		},
		afterInit(swiper) {
			swiper.scrollbar.updateSize()
		}
	}
};

const elementScrollerConfig = {
	direction: 'vertical',
	slidesPerView: 'auto',
	freeMode: true,
	mousewheel: {
		sensitivity: 0.1
	},
	nested: true,
	scrollbar: {
		el: '.swiper-scrollbar--element',
		hide: true,
		draggable: true
	},
	on: {
		scroll(swiper, event) {
			// 
		},
		update(swiper) {
			swiper.scrollbar.updateSize()
		},
		afterInit(swiper) {
			// 
			swiper.scrollbar.updateSize()
		}
	},
};

const mapScrollerConfig = {
	direction: 'horizontal',
	slidesPerView: 'auto',
	freeMode: true,
	simulateTouch: false,
	mousewheel: {
		sensitivity: 0.1
	},
	nested: true,
	scrollbar: {
		el: '.swiper-scrollbar--map',
		hide: false,
		draggable: true
	},
	on: {
		update(swiper) {
			swiper.scrollbar.updateSize()
		},
		afterInit(swiper) {
			swiper.scrollbar.updateSize()
		}
	}
};

const fullPageConfig = {
	direction: 'vertical',
	slidesPerView: 1,
	simulateTouch: false,
	speed: 500,
	mousewheel: true,
	on: {
		slideChange(swiper) {
			const theme = swiper.slides[swiper.activeIndex].dataset.headerTheme;
			const header = document.querySelector('#header');
			header.classList.forEach(selectorClass => {
				if (selectorClass != `header--{theme}` && selectorClass != 'header') {
					header.classList.remove(selectorClass)
				}
			})
			if (!header.classList.contains(`header--${theme}`)) {
				header.classList.add(`header--${theme}`)
			}
		},
		afterInit(swiper) {
			const header = document.querySelector('#header');
			const theme = swiper.slides[swiper.activeIndex].dataset.headerTheme;

			header.classList.forEach(selectorClass => {
				if (selectorClass != `header--{theme}` && selectorClass != 'header') {
					header.classList.remove(selectorClass)
				}
			})
			if (!header.classList.contains(`header--${theme}`)) {
				header.classList.add(`header--${theme}`)
			}

			const footer = document.querySelector('[data-footer]');
			swiper.slides[swiper.slides.length - 1].querySelector('.section__scroll-slide').insertAdjacentElement('beforeend', footer);

		},
	}
};
let fullPageScrollerUpdate;
const fullPageScrollerConfig = {
	direction: 'vertical',
	slidesPerView: 'auto',
	freeMode: true,
	nested: true,
	noSwipingClass: '.select',
	noSwiping: true,
	noSwipingSelector: '.select',
	mousewheel: {
		sensitivity: 0.2
	},
	simulateTouch: false,
	scrollbar: {
		el: '.swiper-scrollbar--section',
		hide: true,
		draggable: true,
	},
	on: {
		scroll(swiper, event) {
			if (swiper.el.querySelector('[data-front-side-animation]')) {
				if (!swiper.el.closest('.section').classList.contains('active')) {
					event.stopPropagation();
				} else {
					if (event.target.closest('.select')) {
						event.stopPropagation();
					}
				}
				if ((event.wheelDelta ? event.wheelDelta < 0 : event.deltaY > 0) && (!swiper.isBeginning || swiper.isEnd) && !event.target.closest('.select')) {
					swiper.el.closest('.section').classList.add('active');
					if (fullPageScrollerUpdate) {
						clearTimeout(fullPageScrollerUpdate)
					}
					fullPageScrollerUpdate = setTimeout(() => {
						swiper.update();
					}, 500)
				} else if ((event.wheelDelta ? event.wheelDelta > 0 : event.deltaY < 0) && swiper.isBeginning && !event.target.closest('.select')) {
					swiper.el.closest('.section').classList.remove('active')
				}
			}
		},
		touchStart(swiper, event) {
			if(swiper.el.querySelector('.map')) {
				if (event.target.closest('.map')) {
					event.stopPropagation();
					swiper.allowTouchMove = false
				} else {
					swiper.allowTouchMove = true
				}
			}
			if(swiper.el.querySelector('[data-presentation]')) {
				if (event.target.closest('[data-presentation]')) {
					event.stopPropagation();
					swiper.allowTouchMove = false
				} else {
					swiper.allowTouchMove = true
				}
			}
		},
		afterInit(swiper) {
			if (swiper.el.querySelector('[data-footer]')) {
				swiper.update()
			}
			swiper.scrollbar.updateSize();
			if (swiper.el.querySelector('[data-front-side-animation]')) {
				document.querySelector('#header').classList.add('header--basic')
			}
		},
		update(swiper) {
			swiper.scrollbar.updateSize()
		}
	}
};

// ===================
// ===================

// function declarations

const initFullPageAndScrollers = () => {
	fullPage = window.fullPage = new Swiper(fullPageSelector, fullPageConfig);
	fullPageScroller = window.fullPageScroller = new Swiper(fullPageScrollerSelector, fullPageScrollerConfig);
	menuScroller = window.menuScroller = new Swiper(menuScrollerSelector, menuScrollerConfig);
	selectScroller = window.selectScroller = new Swiper(selectScrollerSelector, selectScrollerConfig);
	elementScroller = window.elementScroller = new Swiper(elementScrollerSelector, elementScrollerConfig);
	mapScroller = window.mapScroller = new Swiper(mapScrollerSelector, mapScrollerConfig);
}

// ====================
// ====================

// set base config for age-limit and promo-section
// ======================

setPromoAndAgeLimitConfig();

// ======================
// ======================

// on page loaded logic
// ====================

document.addEventListener("DOMContentLoaded", (ev) => {
	var videoPlayer = document.querySelector('[data-video-player]'),
			mapPlayer = document.getElementById('video-map'),
			videojsPlayer, videojsMapPlayer;

	if(videoPlayer) {
		videojsPlayer = videojs(videoPlayer.id);
	}
	if(mapPlayer) {
		videojsMapPlayer = videojs(mapPlayer.id);
	}

	MicroModal.init();
	svg4everybody();
	initFullPageAndScrollers();
	setCarouselLogic(Swiper, carouselEmployees, carouselEmployeesSelector, carouselEmployeesNavNext, carouselEmployeesNavPrev);
	controlSound();
	videoSettings(videojsPlayer);
	toggle((triggerElement, targetElement) => {
		actionsAfterCallConfirmOrRepeatVideo(triggerElement, targetElement, videojsPlayer)
	});

	setPreviewsLogic(fullPageScroller, timer);
	setFrontSideMobileLogic();
	setFoundersLogic();
	setProductLogic(update, fullPageScroller);
	setInputMaskLogic(Inputmask);
	setFooterLogic();
	setSelectLogic(fullPage, fullPageScroller, MicroModal);
	setAccordionLogic(update, fullPageScroller);



	window.addEventListener('resize', (e) => {
		if (update) {
			clearTimeout(update)
		};
		update = setTimeout(() => {
			if (selectScroller && selectScroller.$el) {
				selectScroller.update();
			}
			if (elementScroller && elementScroller.$el) {
				elementScroller.update();
			}
			if (mapScroller && mapScroller.$el) {
				mapScroller.update();
			}
			menuScroller.update();
			fullPage.update();
			if (fullPageScroller && fullPageScroller.length) {
				fullPageScroller.forEach(elem => {
					elem.update()
				});
			} else if (fullPageScroller) {
				fullPageScroller.update()
			}
		}, 500);
	});



	if($ && jQuery) {
			let type = "POST";
			// var shopLat, shopLng;
		
			$('.map-stores__map-link').on('click', function(e){
				e.preventDefault();
				preloader.classList.remove('hidden')
		
				let $this = $(this),
					request = $this.attr('href'),
					region = $.urlParam('region', request),
					data = {};
		
				if ( !navigator.userAgent.match( /msie/i ) ) {
					let title = $('title').text() + ' ' + $this.find('.map-stores__map-tooltip').text();
		
								window.history.pushState({ "pageTitle": title }, title, request );
						}
		
				if ( region ) {
					data.region = region;
		
					$.ajax({
								type: type,
								url: ajax_object.ajax_url,
								data: {
										action: 'get_stores',
										data: data
								},
								beforeSend: function() {
										
								},
								success: function(response) {
										if (response) {
											$('.map-stores__map-link').each(function(i, elem) {
												elem.classList.remove('current')
											});
											$this.addClass('current')
											$('#shops-response').html(response);
											if (update) {
												clearTimeout(update)
											};
											update = setTimeout(() => {
												
												if (elementScroller && elementScroller.$el) {
													elementScroller.update();
												}
												if (fullPageScroller && fullPageScroller.length) {
													fullPageScroller.forEach(elem => {
														elem.update()
													});
												} else if (fullPageScroller) {
													fullPageScroller.update()
												}
												preloader.classList.add('hidden');


												[].slice.call(document.querySelectorAll('[data-map-open]')).forEach((link) => {
													link.addEventListener('click', function(e) {
														e.preventDefault();
														MicroModal.show('modal-partners');
														var newPosition;
														var dest = { lat: e.currentTarget.dataset.lat, lng: e.currentTarget.dataset.lng };
														
														if (navigator.geolocation) {
															navigator.geolocation.getCurrentPosition(
																(position) => {
																	
																	const pos = {
																		lat: position.coords.latitude,
																		lng: position.coords.longitude,
																	};
																	newPosition = pos;
																	
																	partnersOnChangeHandler(newPosition, dest)
																},
																() => {
																	partnersHandleLocationError(true, partnersDirectionInfoWindow, partnersMap.getCenter());
																}
															);
														} else {
															partnersHandleLocationError(false, partnersDirectionInfoWindow, partnersMap.getCenter());
														}
													});
												})
											}, 500);
										}
								}
						});
				}
		
				// return false;
			});
	}


	if(document.querySelector('[data-select]')) {
		document.querySelector('[data-select]').addEventListener('click', e => {
			console.log(e.currentTarget);
			e.currentTarget.classList.toggle('active')
		})
	}

	document.body.addEventListener('click', e => {
		if(document.querySelector('[data-select]')) {
			if(!e.target.closest('[data-select]')) {
				document.querySelector('[data-select]').classList.remove('active');
			}
		}
	});
	document.addEventListener('mouseover', e => {
		let swiperPresentation;
		let swiperMap;
		if(Array.isArray(fullPageScroller)) {
			swiperPresentation = fullPageScroller.find(scroller => scroller.el.querySelector('[data-presentation]'))
			swiperMap = fullPageScroller.find(scroller => scroller.el.querySelector('.map'))
		} else {
			swiperPresentation = fullPageScroller;
			swiperMap = fullPageScroller
		}
		if(e.target.closest('[data-presentation]')) {
			swiperPresentation.mousewheel.disable()
			fullPage.mousewheel.disable()
		}
		if(e.target.closest('.map')) {
			swiperMap.mousewheel.disable()
			fullPage.mousewheel.disable()
		}
	});
	document.addEventListener('mouseout', e => {
		let swiperPresentation;
		let swiperMap;
		if(Array.isArray(fullPageScroller)) {
			swiperPresentation = fullPageScroller.find(scroller => scroller.el.querySelector('[data-presentation]'))
			swiperMap = fullPageScroller.find(scroller => scroller.el.querySelector('.map'))
		} else {
			swiperPresentation = fullPageScroller;
			swiperMap = fullPageScroller
		}
		if(e.target.closest('[data-presentation]')) {
			swiperPresentation.mousewheel.enable()
			fullPage.mousewheel.enable()
		}
		if(e.target.closest('.map')) {
			swiperMap.mousewheel.enable()
			fullPage.mousewheel.enable()
		}
	});
	
	if(videojsPlayer && window.localStorage.legionPromoWatched == 'true') {
		videojsPlayer.muted(true)
		videojsPlayer.play()
	}
	
	preloader.classList.add('hidden');
});

// ====================
// ====================