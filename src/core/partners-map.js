var partnersMap, partnersInfoWindow, partnersDirectionInfoWindow;
var partnersDirectionsService, partnersDirectionsRenderer;
var partnersCities = {
	all: {
		center: { lat: 41.3021592, lng: 60.0851922 },
		zoom: 5,
	},
}
function initPartnersMap() {
	partnersDirectionsService = new google.maps.DirectionsService();
	partnersDirectionsRenderer = new google.maps.DirectionsRenderer();
	partnersMap = new google.maps.Map(document.getElementById("map-modal"), {
		zoom: partnersCities["all"].zoom,
		center: partnersCities["all"].center,
		scrollwheel: false,
		navigationControl: false,
		scaleControl: true,
		draggable: true,
		mapTypeControl: false,
		panControl: false,
		zoomControl: true,
		streetViewControl: false,
		fullscreenControl: false,
		styles: [
			{
					"featureType": "all",
					"elementType": "geometry.fill",
					"stylers": [
							{
									"weight": "2.00"
							}
					]
			},
			{
					"featureType": "all",
					"elementType": "geometry.stroke",
					"stylers": [
							{
									"color": "#9c9c9c"
							}
					]
			},
			{
					"featureType": "all",
					"elementType": "labels.text",
					"stylers": [
							{
									"visibility": "on"
							}
					]
			},
			{
					"featureType": "landscape",
					"elementType": "all",
					"stylers": [
							{
									"color": "#f2f2f2"
							}
					]
			},
			{
					"featureType": "landscape",
					"elementType": "geometry.fill",
					"stylers": [
							{
									"color": "#ffffff"
							}
					]
			},
			{
					"featureType": "landscape.man_made",
					"elementType": "geometry.fill",
					"stylers": [
							{
									"color": "#ffffff"
							}
					]
			},
			{
					"featureType": "poi",
					"elementType": "all",
					"stylers": [
							{
									"visibility": "off"
							}
					]
			},
			{
					"featureType": "road",
					"elementType": "all",
					"stylers": [
							{
									"saturation": -100
							},
							{
									"lightness": 45
							}
					]
			},
			{
					"featureType": "road",
					"elementType": "geometry.fill",
					"stylers": [
							{
									"color": "#eeeeee"
							}
					]
			},
			{
					"featureType": "road",
					"elementType": "labels.text.fill",
					"stylers": [
							{
									"color": "#7b7b7b"
							}
					]
			},
			{
					"featureType": "road",
					"elementType": "labels.text.stroke",
					"stylers": [
							{
									"color": "#ffffff"
							}
					]
			},
			{
					"featureType": "road.highway",
					"elementType": "all",
					"stylers": [
							{
									"visibility": "simplified"
							}
					]
			},
			{
					"featureType": "road.arterial",
					"elementType": "labels.icon",
					"stylers": [
							{
									"visibility": "off"
							}
					]
			},
			{
					"featureType": "transit",
					"elementType": "all",
					"stylers": [
							{
									"visibility": "off"
							}
					]
			},
			{
					"featureType": "water",
					"elementType": "all",
					"stylers": [
							{
									"color": "#46bcec"
							},
							{
									"visibility": "on"
							}
					]
			},
			{
					"featureType": "water",
					"elementType": "geometry.fill",
					"stylers": [
							{
									"color": "#c8d7d4"
							}
					]
			},
			{
					"featureType": "water",
					"elementType": "labels.text.fill",
					"stylers": [
							{
									"color": "#070707"
							}
					]
			},
			{
					"featureType": "water",
					"elementType": "labels.text.stroke",
					"stylers": [
							{
									"color": "#ffffff"
							}
					]
			}
		]
	});
	partnersDirectionsRenderer.setMap(partnersMap);
};
var partnersOnChangeHandler = function (position, dest) {
	partnersCalculateAndDisplayRoute(partnersDirectionsService, partnersDirectionsRenderer, position, dest);
};
function partnersCalculateAndDisplayRoute(partnersDirectionsService, partnersDirectionsRenderer, position, dest) {
	
	
  partnersDirectionsService.route({
		origin: new google.maps.LatLng(position.lat, position.lng),
		destination: new google.maps.LatLng(dest.lat, dest.lng),
		travelMode: google.maps.TravelMode.DRIVING,
	}, (response, status) => {
		
      if (status === "OK") {
        partnersDirectionsRenderer.setDirections(response);
      } else {
        window.alert("Directions request failed due to " + status);
      }
    }
  );
};
function partnersHandleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(
    browserHasGeolocation
      ? "Error: The Geolocation service failed."
      : "Error: Your browser doesn't support geolocation."
  );
  infoWindow.open(partnersMap);
}


document.addEventListener("DOMContentLoaded", () => {
	if (document.querySelector('#map-modal')) {
		console.log('modal-map found');
		initPartnersMap();
		partnersDirectionInfoWindow = new google.maps.InfoWindow();
		if(document.querySelector('[data-map-open]')) {
			[].slice.call(document.querySelectorAll('[data-map-open]')).forEach((link) => {
				link.addEventListener('click', function(e) {
					e.preventDefault();
					MicroModal.show('modal-partners');
					var newPosition;
					var dest = { lat: e.currentTarget.dataset.lat, lng: e.currentTarget.dataset.lng };
					
					if (navigator.geolocation) {
						navigator.geolocation.getCurrentPosition(
							(position) => {
								
								const pos = {
									lat: position.coords.latitude,
									lng: position.coords.longitude,
								};
								newPosition = pos;
								
								partnersOnChangeHandler(newPosition, dest)
							},
							() => {
								partnersHandleLocationError(true, partnersDirectionInfoWindow, partnersMap.getCenter());
							}
						);
					} else {
						partnersHandleLocationError(false, partnersDirectionInfoWindow, partnersMap.getCenter());
					}
				});
			})
		}
		
	}
});