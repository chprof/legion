var map, markers = [], image, imageActive, shape, infoWindow, markerWindow, directionsService, directionsRenderer;
	var cities = {
		tashkent: {
			center: { lat: 41.2827052, lng: 69.2093271 },
			zoom: 11,
		},
	};
	var offices = [
		{
			officeName: '#1',
			center: { lat: 41.2849176, lng: 69.2052416 },
			phone: '998991112233',
			address: 'Lorem ipsum dolor sit amet.',
			img: 'img/store1.jpg',
			schedule: 'пн-пт, 9:00-20:00'
		},
	]

	// offices = offices.map(function(oldOffice) {
	// 	return {
	// 		address: oldOffice.address,
	// 		center: { lat: +oldOffice.center.lat, lng: +oldOffice.center.lng },
	// 		// img: oldOffice.img,
	// 		// phone: oldOffice.phone,
	// 		// schedule: oldOffice.schedule,
	// 		// storeName: oldOffice.storeName
	// 	}
	// });
	// Object.keys(cities).forEach(function(city) {
	// 	cities[city].center = { lat: +cities[city].center.lat, lng: +cities[city].center.lng };
	// 	cities[city].zoom = +cities[city].zoom
	// });
	function initMap() {
		directionsService = new google.maps.DirectionsService();
  	directionsRenderer = new google.maps.DirectionsRenderer();
		map = new google.maps.Map(document.getElementById("map"), {
			zoom: cities["tashkent"].zoom,
			center: cities["tashkent"].center,
			scrollwheel: false,
			navigationControl: false,
			scaleControl: true,
			draggable: true,
			mapTypeControl: false,
			panControl: false,
			zoomControl: true,
			streetViewControl: false,
			fullscreenControl: false,
			styles: [
				{
					"featureType": "all",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"weight": "2.00"
						}
					]
				},
				{
					"featureType": "all",
					"elementType": "geometry.stroke",
					"stylers": [
						{
							"color": "#9c9c9c"
						}
					]
				},
				{
					"featureType": "all",
					"elementType": "labels.text",
					"stylers": [
						{
							"visibility": "on"
						}
					]
				},
				{
					"featureType": "landscape",
					"elementType": "all",
					"stylers": [
						{
							"color": "#f2f2f2"
						}
					]
				},
				{
					"featureType": "landscape",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#ffffff"
						}
					]
				},
				{
					"featureType": "landscape.man_made",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#ffffff"
						}
					]
				},
				{
					"featureType": "poi",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "all",
					"stylers": [
						{
							"saturation": -100
						},
						{
							"lightness": 45
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#eeeeee"
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"color": "#7b7b7b"
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "labels.text.stroke",
					"stylers": [
						{
							"color": "#ffffff"
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "simplified"
						}
					]
				},
				{
					"featureType": "road.arterial",
					"elementType": "labels.icon",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "transit",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "all",
					"stylers": [
						{
							"color": "#46bcec"
						},
						{
							"visibility": "on"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#c8d7d4"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"color": "#070707"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "labels.text.stroke",
					"stylers": [
						{
							"color": "#ffffff"
						}
					]
				}
			]
		});
		directionsRenderer.setMap(map);
		var onChangeHandler = function (position) {
			calculateAndDisplayRoute(directionsService, directionsRenderer, position);
		};
		infoWindow = new google.maps.InfoWindow();
		markerWindow = new google.maps.InfoWindow();
		if (document.querySelector('[data-direction-button]')) {
			var locationButtons = document.querySelectorAll('[data-direction-button]');
			[].slice.call(locationButtons).forEach(function (locationButton) {
				locationButton.addEventListener("click", (e) => {
					e.preventDefault();
					var newPosition;
					if (navigator.geolocation) {
						navigator.geolocation.getCurrentPosition(
							(position) => {
								
								const pos = {
									lat: position.coords.latitude,
									lng: position.coords.longitude,
								};
								newPosition = pos;
								
								// onChangeHandler(newPosition)
								let new_window = window.open('https://maps.google.com/maps?saddr=' + pos.lat + ',' + pos.lng + '&daddr=' + offices[0].center.lat + ',' + offices[0].center.lng)
							},
							() => {
								handleLocationError(true, infoWindow, map.getCenter());
							}
						);
					} else {
						handleLocationError(false, infoWindow, map.getCenter());
					}
				});
			});
		}
		setMarkers(map);
	};
	function calculateAndDisplayRoute(directionsService, directionsRenderer, position) {
		console.log(position);
		directionsService.route(
			{
				origin: new google.maps.LatLng(position.lat, position.lng),
				destination: new google.maps.LatLng(offices[0].center.lat, offices[0].center.lng),
				travelMode: google.maps.TravelMode.DRIVING,
			},
			(response, status) => {
				if (status === "OK") {
					directionsRenderer.setDirections(response);
				} else {
					window.alert("Directions request failed due to " + status);
				}
			}
		);
	};
	function setMarkers(map) {
		image = {
			// url: "/app/themes/legion-theme/assets/img/icons/loc-grey.svg",
			url: "img/icons/loc-grey.svg",
			size: new google.maps.Size(26, 42),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(11, 42),
		};
		imageActive = {
			// url: "/app/themes/legion-theme/assets/img/icons/loc-red.svg",
			url: "img/icons/loc-red.svg",
			size: new google.maps.Size(26, 42),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(11, 42),
		};
		shape = {
			coords: [0, 0, 26, 0, 26, 42, 0, 42, 0, 0],
			type: "poly",
		};
		var marker;
		offices.forEach(function (office, i) {
			marker = new google.maps.Marker({
				position: office.center,
				map,
				icon: image,
				shape: shape,
				// phone: office.phone,
				address: office.address,
				title: office.address
			});
			markers.push(marker);
			google.maps.event.addListener(marker, 'click', (function (marker, i) {
				return function () {
					var infoMarkerString = `<div class="marker-info">
						<div class="marker-info__address">${office.address}</div>
					</div>`
					markerWindow.setContent(infoMarkerString);
					markerWindow.open(map, marker, infoMarkerString);
					markers.forEach(function (regularMarker, j) {
						regularMarker.setIcon(image);
					})
					this.setIcon(imageActive);
				}
			})(marker, i))
		})
	};
	function handleLocationError(browserHasGeolocation, infoWindow, pos) {
		infoWindow.setPosition(pos);
		infoWindow.setContent(
			browserHasGeolocation
				? "Error: The Geolocation service failed."
				: "Error: Your browser doesn't support geolocation."
		);
		infoWindow.open(map);
	}

	document.addEventListener("DOMContentLoaded", () => {
		if (document.querySelector('#map')) {
			console.log('map found');
			initMap();
		}
	});