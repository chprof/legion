export default () => {
  const soundCheckElement = document.getElementById('soundCheck');
	if(soundCheckElement) {
		soundCheckElement.addEventListener('change', e => {
			e.currentTarget.checked ? window.localStorage.legionPromoSound = true : window.localStorage.legionPromoSound = false;
		})
	}
}