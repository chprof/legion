export default () => {
	let current_time = new Date().getTime();
	// current_time += 86400000;
	if (window.localStorage.getItem('updateTime') && ((current_time - +window.localStorage.getItem('updateTime')) / 86400000) >= 1) {
		window.localStorage.setItem('legionVisited', 'false')
		window.localStorage.setItem('legionPromoSound', 'true')
		// current_time -= 86400000;
		window.localStorage.setItem('updateTime', String(current_time))
	} else {
		window.localStorage.setItem('updateTime', String(current_time))
	}

	if (window.localStorage.getItem('legionVisited') === 'true') {
		if (document.querySelector('#age-limit')) {
			document.querySelector('#age-limit').classList.add('hidden')
		}
	}
	if (window.localStorage.getItem('legionPromoSound') === 'false') {

		if (document.querySelector('.video__volume-button') && document.querySelector('[data-video-player]')) {
			document.querySelector('.video__volume-button').classList.add('muted');
			document.querySelector('[data-video-player]').muted = true;
		}
	} else {


		if (document.querySelector('.video__volume-button') && document.querySelector('[data-video-player]')) {

			document.querySelector('.video__volume-button').classList.remove('muted');
			document.querySelector('[data-video-player]').muted = false;
		}
	}
}