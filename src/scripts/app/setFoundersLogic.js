export default () => {
	if(document.querySelector('[data-founder-toggle]')) {
		[].slice.call(document.querySelectorAll('[data-founder-toggle]')).forEach(toggle => {
			toggle.addEventListener('click', (e) => {
				e.preventDefault();
				const _this = e.currentTarget;
				const target = _this.dataset.target;
				[].slice.call(document.querySelectorAll('[data-founder-toggle]')).forEach(toggle => {
					toggle.classList.remove('active');
				});
				[].slice.call(document.querySelector(target).closest('[data-founder-frame]').children).forEach(item => {
					item.classList.remove('active');
				})
				document.querySelector(target).classList.add('active');
				_this.classList.add('active')
			})
		})

	}
}