import controlFullScreen from './controlFullScreen'

export default function promoSettings(videojsPlayer) {
	const volume = document.querySelector('.video__volume-button');
	if(videojsPlayer) {
		videojsPlayer.on('play', e => {
			
			if(videojsPlayer.muted()) {
				volume.classList.add('muted')
			}
			if(e.type == 'play') {
				window.localStorage.setItem('legionPromoWatched', true)
			}
		})
	
		videojsPlayer.on('ended', e => {
			
			window.localStorage.setItem('legionPromoSound', false)
			if(videojsPlayer.muted() == false) {
				videojsPlayer.muted(true);
			}
			videojsPlayer.play()
		});
	
		volume.addEventListener('click', e => {
			if(e.currentTarget.classList.contains('muted')) {
				window.localStorage.setItem('legionPromoSound', true)
				videojsPlayer.muted(false);
				e.currentTarget.classList.remove('muted')
			} else {
				window.localStorage.setItem('legionPromoSound', false)
				videojsPlayer.muted(true);
				e.currentTarget.classList.add('muted')
			}
		})
	}
}