export default (Inputmask) => {
	const inputTel = document.querySelectorAll("input[type='tel']");
	const input_mask = new Inputmask("+\\9\\98(99) 999 99 99");
	[].slice.call(inputTel).forEach(input => {
		input_mask.mask(input);
	})
}