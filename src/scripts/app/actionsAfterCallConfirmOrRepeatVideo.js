export default (triggerElement, targetElement, videojsPlayer) => {
	if(targetElement.id == 'age-limit') {
		window.localStorage.legionVisited = true
		if(videojsPlayer && document.querySelector('.video__volume-button')) {
			if(window.localStorage.getItem('legionPromoSound') === 'true') {
				document.querySelector('.video__volume-button').classList.remove('muted')
				videojsPlayer.muted(false)
			} else {
				document.querySelector('.video__volume-button').classList.add('muted')
				videojsPlayer.muted(true)
			}
			videojsPlayer.play();
		}
	}
}