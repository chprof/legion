export default (update, fullPageScroller ) => {
	if(document.querySelector('[data-collapse-product]')) {
		let expanded = false;
		[].slice.call(document.querySelectorAll('[data-collapse-product]')).forEach((_btn, idx) => {
			_btn.addEventListener('click', (e) => {
				e.preventDefault();
				const otherElems = [].slice.call(document.querySelectorAll('[data-collapse-product]')).filter((elem, index) => index != idx);
				otherElems.forEach(elem => {
					elem.classList.remove('active');
					elem.classList.add('disabled');
					document.querySelector(elem.getAttribute('href')).classList.remove('show');
					elem.style.marginBottom = 0;
					if(update) {
						clearTimeout(update)
					};
					update = setTimeout(() => {
						fullPageScroller.forEach(elem => elem.update());
					}, 400);
				})
				const _this = e.currentTarget;
				const _thisCollapse = document.querySelector(_this.getAttribute('href'));
				const _thisScroller = _this.closest('[data-full-page-scroller]');
				let _thisRect = _this.getBoundingClientRect();
				let _thisCollapseRect = _thisCollapse.getBoundingClientRect();
				let _containerRect = _this.closest('[data-collapse-container]').getBoundingClientRect();
				let _sectionRect = _this.closest('[data-full-page-scroller]').getBoundingClientRect();
				
				if(_this.classList.contains('active')) {
					_this.classList.remove('active');
					_thisCollapse.classList.remove('show');
					_this.style.marginBottom = 0;
					otherElems.forEach(elem => {
						elem.classList.remove('disabled')
					})
					expanded = false
					
					if(update) {
						clearTimeout(update)
					};
					update = setTimeout(() => {
						fullPageScroller.forEach(elem => elem.update());
					}, 600);
				} else {
					if(!expanded) {
						_this.classList.add('active');
						_this.classList.remove('disabled');
						_thisCollapse.classList.add('show');
						_this.style.marginBottom = _thisCollapseRect.height + 150 + 'px';
						_thisCollapse.style.top = _thisRect.top - _containerRect.top + _thisRect.height + 30 + 'px';
						expanded = true
					} else {
						setTimeout(() => {
							_thisRect = _this.getBoundingClientRect();
							_thisCollapseRect = _thisCollapse.getBoundingClientRect();
							_containerRect = _this.closest('[data-collapse-container]').getBoundingClientRect();
							_sectionRect = _this.closest('[data-full-page-scroller]').getBoundingClientRect();
							
							_this.classList.add('active');
							_this.classList.remove('disabled');
							_thisCollapse.classList.add('show');
							_this.style.marginBottom = _thisCollapseRect.height + 150 + 'px';
							_thisCollapse.style.top = _thisRect.top - _containerRect.top + _thisRect.height + 30 + 'px';
						}, 400)
					}
					
					if(update) {
						clearTimeout(update)
					};
					update = setTimeout(() => {
						console.log(fullPageScroller);
						// fullPageScroller.forEach(elem => elem.update());
						fullPageScroller.update();
					}, 600);
				}
			})
		})
	};
}