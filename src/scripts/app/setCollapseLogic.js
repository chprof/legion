export default () => {
  [].slice.call(document.querySelectorAll('[data-collapse-toggle]')).forEach(collapse => {
    
    
    if(window.innerWidth < 1500) {
      collapse.classList.remove('expanded');
      collapse.nextElementSibling.classList.add('hidden');
      
      collapse.addEventListener('click', (e) => {
        const _this = e.currentTarget;
        const _collapse = _this.nextElementSibling;
        const parent = _this.closest(_this.dataset.collapseParent);
        const allCollapse = parent.querySelectorAll('[data-collapse-toggle]');
        
        
        [].slice.call(allCollapse).forEach(collapseToggle => {
          if(collapseToggle.nextElementSibling.offsetHeight != 0) {
            
            collapseToggle.nextElementSibling.style.maxHeight = 0 + 'px';
          } 
          if(_this === collapseToggle) {
            
            if(collapseToggle.nextElementSibling.offsetHeight != 0) {
              collapseToggle.nextElementSibling.style.maxHeight = 0 + 'px';
            } else {
              
              // 
              if(collapseToggle.nextElementSibling && collapseToggle.nextElementSibling.firstElementChild) {
                collapseToggle.nextElementSibling.style.maxHeight = collapseToggle.nextElementSibling.firstElementChild.offsetHeight + 'px';
              }
            }
            // collapseToggle.nextElementSibling.style.maxHeight = 0 + 'px'
            // collapseToggle.nextElementSibling.classList.add('hidden');
          }
        })
        
        // if(_collapse.style.offsetHeight == 0) {
          
        //   _this.classList.add('expanded');
        //   _collapse.classList.remove('hidden');
        //   _collapse.style.maxHeight = _collapse.firstElementChild.offsetHeight + 'px';
        // } else {
          
        //   _this.classList.remove('expanded');
        //   _collapse.classList.add('hidden');
        //   _collapse.style.maxHeight = 0;
        // }
      })
    }
  })
}