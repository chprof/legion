export default (fullScreenTarget, triggerElem, triggerActiveClass) => {
  if(triggerElem.classList.contains(triggerActiveClass)) {
    if (fullScreenTarget.requestFullscreen) {
      fullScreenTarget.requestFullscreen();
    } else if (fullScreenTarget.webkitRequestFullscreen) {
      fullScreenTarget.webkitRequestFullscreen();
    } else if (fullScreenTarget.msRequestFullScreen) {
      fullScreenTarget.msRequestFullScreen();
    }
  } else {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    }
  }
}