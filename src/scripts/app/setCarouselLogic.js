export default (Swiper, carouselEmployees, carouselEmployeesSelector, carouselEmployeesNavNext, carouselEmployeesNavPrev) => {
	carouselEmployees = new Swiper(carouselEmployeesSelector, {
		slidesPerView: 'auto',
		spaceBetween: 0,
		updateOnWindowResize: true,
		navigation: {
			nextEl: carouselEmployeesNavNext,
			prevEl: carouselEmployeesNavPrev,
			clickable: true,
		},
		breakpoints: {
			0: {
				slidesPerView: 1,
				spaceBetween: 20
			},
			992: {
				slidesPerView: 'auto',
				spaceBetween: 0
			}
		}
	});

	if(document.querySelector('[data-carousel-slide]') && window.innerWidth > 991) {
		[].slice.call(document.querySelectorAll('[data-carousel-slide]')).forEach((slide, slideIdx) => {
			slide.addEventListener('click', (e) => {
				const _this = e.currentTarget,
					_thisRect = _this.getBoundingClientRect(),
					_parent = _this.closest('[data-carousel-employees]'),
					_parentRect = _parent.getBoundingClientRect(),
					_expand = _this.querySelector('[data-carousel-slide-expand]'),
					_prev = document.querySelector('[data-carousel-employees-prev]'),
					_next = document.querySelector('[data-carousel-employees-next]'),
					prevSlides = [].slice.call(document.querySelectorAll('[data-carousel-slide]')).filter((slidePrev, slidePrevIdx) => slidePrevIdx < slideIdx),
					nextSlides = [].slice.call(document.querySelectorAll('[data-carousel-slide]')).filter((slideNext, slideNextIdx) => slideNextIdx > slideIdx);

				prevSlides.forEach(prevSlide => {
					prevSlide.classList.add('carousel__slide--hide-prev');
				})
				nextSlides.forEach(nextSlide => {
					nextSlide.classList.add('carousel__slide--hide-next');
				})

				
				_this.classList.add('carousel__slide--expanded');
				
				if(carouselEmployees.getTranslate() < 0) {
					_this.style.marginLeft = Math.abs(carouselEmployees.getTranslate()) +'px';
				}
				carouselEmployees.allowTouchMove = false;
				_prev.classList.add('animated-hidden');
				_next.classList.add('animated-hidden');

			})
		})
	}
	if(document.querySelector('[data-carousel-close]') && window.innerWidth > 991) {
		
		[].slice.call(document.querySelectorAll('[data-carousel-close]')).forEach(close => {
			close.addEventListener('click', e => {
				e.stopPropagation();

				const _this = e.currentTarget.closest('[data-carousel-slide]'),
					_parent = document.querySelector('[data-carousel-employees]'),
					_prev = document.querySelector('[data-carousel-employees-prev]'),
					_next = document.querySelector('[data-carousel-employees-next]');
				
				[].slice.call(document.querySelectorAll('[data-carousel-slide]')).forEach(slide => {
					slide.classList.remove('carousel__slide--hide-prev', 'carousel__slide--hide-next', 'carousel__slide--expanded');
				});

				carouselEmployees.allowTouchMove = true;
				_this.style.marginLeft = 0;
				_prev.classList.remove('animated-hidden');
				_next.classList.remove('animated-hidden');
			})
		})
	}
	// window.addEventListener('resize', (e) => {
	// 	if(update) {
	// 		clearTimeout(update)
	// 	};
	// 	update = setTimeout(() => {
			
	// 		[].slice.call(document.querySelectorAll('[data-carousel-slide]')).forEach((slide, slideIdx) => {
	// 			if(slide.classList.contains('carousel__slide--expanded')) {
					
	// 				const _thisRect = slide.getBoundingClientRect(),
	// 					_parent = slide.parentNode,
	// 					_parentRect = slide.parentNode.getBoundingClientRect(),
	// 					_expand = slide.querySelector('[data-carousel-slide-expand]');
						
	// 				_expand.style.left = _parentRect.left - _thisRect.left + 'px';
	// 				_expand.style.right = _thisRect.right - _parentRect.right + 15 + 'px';
	// 			}
	// 		})
	// 	}, 200);
	// });
}