import setAccordionLogic from './setAccordionLogic';
let update;
let accordionUpdate;
export default (fullPage, fullPageScroller, MicroModal) => {
	if(document.querySelector('[data-select-link]')) {
		[].slice.call(document.querySelectorAll('[data-select-link]')).forEach((link, index) => {
			link.addEventListener('click', e => {
				e.preventDefault();
				const _this = e.currentTarget;
				const _others = [].slice.call(document.querySelectorAll('[data-select-link]')).filter((otherLink, idx) => index !== idx);
				[].slice.call(_others).forEach(otherLink => {
					otherLink.classList.remove('active')
				})
				_this.closest('.select').querySelector('.select__btn-txt').textContent = _this.textContent;
				_this.classList.add('active');
				document.querySelector('.preloader').classList.remove('hidden')
				if(!_this.closest('.section').classList.contains('active')) {
					_this.closest('.section').classList.add('active');
				}
				
				if(jQuery && _this.classList.contains('active')) {
					let text = _this.textContent,
							data = {};
					if(text) {
						data.name = text;
						$.ajax({
							type: 'POST',
							url: ajax_object.ajax_url,
							data: {
								action: 'get_vacancies',
		            data: data
							},
							success: function(response) {
								$('.accordion--basic').html(response);
								if(update) {
									clearTimeout(update)
								};
								update = setTimeout(() => {
									if(fullPageScroller && fullPageScroller.length) {
										fullPageScroller.forEach(elem => elem.update());
									} else if(fullPageScroller) {
										fullPageScroller.update()
									}
								}, 1000);
								if(accordionUpdate) {
									clearTimeout(update)
								};
								accordionUpdate = setTimeout(() => {
									setAccordionLogic(update, fullPageScroller);
									MicroModal.init()
								}, 1000);
								document.querySelector('.preloader').classList.add('hidden')
								
							}
						})
					}
				}
			})
		})
	};
}