export default function attachCustomCursor() {
	const areas = document.querySelectorAll('[data-custom-cursor-area]');
	areas.forEach(area => {
		
		area.addEventListener('mousemove', e => {
			
			const customCursor = area.querySelector('[data-custom-cursor]');
			const customCursorWidth = customCursor.offsetWidth;
			const customCursorHeight = customCursor.offsetHeight;
			
			if(window.innerWidth > 1200) {
				if(!e.target.closest('[data-custom-cursor-ignored]')) {
					customCursor.classList.remove('hidden');
					customCursor.style.top = parseInt(e.pageY - customCursorHeight / 2) + 'px';
					customCursor.style.left = parseInt(e.pageX - customCursorWidth / 2) + 'px';
				} else {
					customCursor.classList.add('hidden');
				}
			}
		})
	})
}