export default (callback = null) => {
	const toggles = document.querySelectorAll('[data-toggle]');

	toggles.forEach((toggle, index) => {
		toggle.addEventListener('click', e => {
			e.preventDefault();
			
			const _this = e.currentTarget;
			const targetName = _this.dataset.target;
			const className = _this.dataset.setClass;
			document.querySelector(targetName).classList.toggle(className);
			if(callback) callback(_this, document.querySelector(targetName), index);
		})
	})
}