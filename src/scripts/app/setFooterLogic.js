export default () => {
  [].slice.call(document.querySelectorAll('[data-footer-dropdown]')).forEach(dropdown => {
    if(dropdown.querySelector('[data-footer-dropdown-drop]')) {
      dropdown.querySelector('[data-footer-dropdown-btn]').classList.add('has-dropdown')
    }
  })
}