export default (update, fullPageScroller) => {
	let newUpdate;
	if(document.querySelector('[data-accordion-toggle]')) {
		[].slice.call(document.querySelectorAll('[data-accordion-toggle]')).forEach((accordionToggle, idx) => {
			const _this = accordionToggle;
			const _parent = _this.closest('[data-accordion-item]')
			const _collapse = _parent.querySelector('[data-accordion-collapse]');
			if(_parent.classList.contains('active')) {
				if(newUpdate) {
					clearTimeout(newUpdate)
				};
				newUpdate = setTimeout(() => {
					_collapse.style.maxHeight = _collapse.firstElementChild.getBoundingClientRect().height + 'px';
				}, 500);
				if(update) {
					clearTimeout(update)
				};
				update = setTimeout(() => {
					if(fullPageScroller && fullPageScroller.length) {
						fullPageScroller.forEach(elem => elem.update());
					} else if(fullPageScroller) {
						fullPageScroller.update()
					}
				}, 1000);
			};
			accordionToggle.addEventListener('click', e => {
				const _this = e.currentTarget;
				const _parent = _this.closest('[data-accordion-item]')
				const _collapse = _parent.querySelector('[data-accordion-collapse]');
				const otherToggles = [].slice.call(document.querySelectorAll('[data-accordion-toggle]')).filter((otherToggle, new_idx) => idx !== new_idx);
				otherToggles.forEach(otherToggle => {
					const _parent = otherToggle.closest('[data-accordion-item]');
					if(_parent.classList.contains('active')) {
						const _collapse = _parent.querySelector('[data-accordion-collapse]');
						_parent.classList.remove('active');
						_collapse.style.maxHeight = 0;
					}
				})

				if(!_parent.classList.contains('active')) {
					_parent.classList.add('active');
					if(_collapse) {
						_collapse.style.maxHeight = _collapse.firstElementChild.getBoundingClientRect().height + 'px';
					}
				} else {
					_parent.classList.remove('active');
					if(_collapse) {
						_collapse.style.maxHeight = 0;
					}
				}
				if(update) {
					clearTimeout(update)
				};
				update = setTimeout(() => {
					if(fullPageScroller && fullPageScroller.length) {
						fullPageScroller.forEach(elem => elem.update());
					} else if(fullPageScroller) {
						fullPageScroller.update()
					}
				}, 500);
			})
		})
	}
}