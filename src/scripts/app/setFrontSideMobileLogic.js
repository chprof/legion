export default () => {
	if(document.querySelector('[data-content-toggle]')) {
		document.querySelector('[data-content-toggle]').addEventListener('click', (e) => {
			e.preventDefault();
			if(window.innerWidth < 1200) {
				const _this = e.currentTarget;
				_this.closest('.section').classList.toggle('active');
				fullPageScroller.forEach(scroller => {
					if(scroller.el.closest('.section') === _this.closest('.section')) {
						scroller.update()
					}
				});
			}
		})
	};
}