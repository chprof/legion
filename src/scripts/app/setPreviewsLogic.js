export default (fullPageScroller, timer) => {
  const showCollapsePreview = (event, elem, index) => {
    document.querySelector(elem.dataset.parent).classList[event]('section--previews-full-screen');
    document.querySelector(elem.dataset.preview).parentNode.classList[event]('section__column-previews--full-screen');
    // elem.classList[event]('preview--full-screen');
    elem.classList.remove(
      'preview--hovered',
      'preview--unhovered'
    )
    if(event == 'add') {
      if(window.innerWidth >= 992) {
        const stepPrimary = (() => {
          let arr = [], i = 0;
          while (i <= elem.parentNode.parentNode.children.length) {
            if((3 * i - 1) >= 0) {
              arr.push(3 * i - 1);
            }
            i++;
          }
          return arr
        })();
        const stepSecondary = (() => {
          let arr = [], i = 0;
          while (i <= elem.parentNode.parentNode.children.length) {
            if((3 * i + 1) >= 0) {
              arr.push(3 * i + 1);
            }
            i++;
          }
          return arr
        })();
        const stepThird = (() => {
          let arr = [], i = 0;
          while (i <= elem.parentNode.parentNode.children.length) {
            if((3 * i) > 0) {
              arr.push(3 * i);
            }
            i++;
          }
          return arr
        })();
        
        
        
        [].slice.call(elem.parentNode.parentNode.children).forEach((previewColumn, index, array) => {
          if(previewColumn.classList.contains('section__column-previews--full-screen')) {
            if(stepPrimary.indexOf(index + 1) >= 0) {
              stepPrimary.forEach(step => {
                if(step == index + 1) {
                  if(array[index - 1]) {
                    array[index - 1].classList[event]('section__column-previews--prev-element-hide')
                  } 
                  if(array[index + 1]) {
                    array[index + 1].classList[event]('section__column-previews--next-element-hide')
                  }
                }
              });
              const newArr = array.filter((column, idx) => idx != index && idx != (index + 1) && idx != (index - 1))
              console.log(newArr);
              newArr.forEach(column => column.classList[event]('section__column-previews--hide'))
            }
            if(stepSecondary.indexOf(index + 1) >= 0) {
              stepSecondary.forEach(step => {
                if(step == index + 1) {
                  if(array[index + 1]) {
                    array[index + 1].classList[event]('section__column-previews--next-element-hide')
                  } 
                  if(array[index + 2]) {
                    array[index + 2].classList[event]('section__column-previews--next-element-hide')
                  }
                }
              });
              const newArr = array.filter((column, idx) => idx != index && idx != (index + 1) && idx != (index + 2))
              console.log(newArr);
              newArr.forEach(column => column.classList[event]('section__column-previews--hide'))
            }
            if(stepThird.indexOf(index + 1) >= 0) {
              stepThird.forEach(step => {
                if(step == index + 1) {
                  if(array[index - 1]) {
                    array[index - 1].classList[event]('section__column-previews--prev-element-hide')
                  } 
                  if(array[index - 2]) {
                    array[index - 2].classList[event]('section__column-previews--prev-element-hide')
                  }
                }
              });
              const newArr = array.filter((column, idx) => idx != index && idx != (index - 1) && idx != (index - 2))
              console.log(newArr);
              newArr.forEach(column => {
                console.log(column);
                // column.classList['add']('section__column-previews--hide')
                column.classList[event]('section__column-previews--hide')

              })
            }
          }
        })
      } else {
        [].slice.call(elem.parentNode.parentNode.children).forEach((previewColumn, index, array) => {
          if(!previewColumn.classList.contains('section__column-previews--full-screen')) {
            previewColumn.classList[event]('section__column-previews--hide')
          }
        })
      }
    }
    if(event == 'remove') {
      if(window.innerWidth >= 992) {
        [].slice.call(elem.closest('.section__row-previews').children).forEach(elem => {
          elem.classList[event](
            'section__column-previews--full-screen',
            'section__column-previews--prev-element-hide',
            'section__column-previews--next-element-hide',
            'section__column-previews--hide'
          )
        })
      } else {
        [].slice.call(elem.closest('.section__row-previews').children).forEach(elem => {
          elem.classList[event](
            'section__column-previews--full-screen',
            'section__column-previews--hide'
          )
        })
      }
    }
    if(fullPageScroller && fullPageScroller.length) {
      fullPageScroller.forEach(scroller => {
        if(scroller.el.closest(elem.dataset.parent)) {
          if(window.innerWidth >= 992) {
            // scroller.translateTo(0, 100, false, false);
            if(timer) {
              clearTimeout(timer)
            }
            timer = setTimeout(() => {
              scroller.update();
            }, 2000)
          } else {
            // scroller.translateTo(0, 0, false, false);
            if(timer) {
              clearTimeout(timer)
            }
            timer = setTimeout(() => {
              scroller.update();
            }, 0)
          }
        }
      });
    } else if(fullPageScroller) {
      if(fullPageScroller.el.closest(elem.dataset.parent)) {
        if(window.innerWidth >= 992) {
          // scroller.translateTo(0, 100, false, false);
          if(timer) {
            clearTimeout(timer)
          }
          timer = setTimeout(() => {
            fullPageScroller.update();
          }, 2000)
        } else {
          // fullPageScroller.translateTo(0, 0, false, false);
          if(timer) {
            clearTimeout(timer)
          }
          timer = setTimeout(() => {
            fullPageScroller.update();
          }, 0)
        }
      }
    }
  };
  
  document.querySelectorAll('[data-collapse-preview]').forEach((elem, index) => {
    elem.addEventListener('click', (e) => {
      e.preventDefault();
      e.stopPropagation();
      if(e.currentTarget.dataset.collapsePreview == 'show') {
        showCollapsePreview('add', e.currentTarget, index);
      } else if(e.currentTarget.dataset.collapsePreview == 'hide') {
        showCollapsePreview('remove', e.currentTarget, index);
      }
    })
  });
  document.querySelectorAll('[data-collapse-preview]').forEach((elem, index) => {
    elem.addEventListener('mouseover', (e) => {
      if(!e.currentTarget.parentNode.classList.contains('section__column-previews--full-screen') && window.innerWidth >= 992) {
        e.currentTarget.classList.remove('preview--unhovered')
        e.currentTarget.classList.add('preview--hovered')
      }
    })
  });
  document.querySelectorAll('[data-collapse-preview]').forEach((elem, index) => {
    elem.addEventListener('mouseout', (e) => {
      if(!e.currentTarget.parentNode.classList.contains('section__column-previews--full-screen') && window.innerWidth >= 992) {
        e.currentTarget.classList.remove('preview--hovered')
        e.currentTarget.classList.add('preview--unhovered')
      }
      
    })
  });


  let animationTimer;
  [].slice.call(document.querySelectorAll('[data-preview-next]')).forEach(nextButton => {
    nextButton.addEventListener('click', (e) => {
      e.preventDefault();
      e.stopPropagation();
      const _this = e.currentTarget;
      const column = _this.closest('.column');
      const columns = _this.closest(_this.dataset.previewsList).children;
      const section = _this.closest(_this.dataset.previewsSection);
      
      [].slice.call(columns).forEach(previewColumn => {
        section.classList.remove('section--previews-full-screen');
        previewColumn.classList.remove(
          'section__column-previews--full-screen',
          'section__column-previews--prev-element-hide',
          'section__column-previews--next-element-hide',
          'section__column-previews--hide'
        );
      });
      if(window.innerWidth >= 992) {
        if(animationTimer) {
          clearTimeout(animationTimer)
        }
        animationTimer = setTimeout(() => {
          if(column.nextElementSibling) {
            column.nextElementSibling.firstElementChild.click()
          } else {
            columns[0].firstElementChild.click();
          }
        }, 3000)
      } else {
        if(animationTimer) {
          clearTimeout(animationTimer)
        }
        animationTimer = setTimeout(() => {
          if(column.nextElementSibling) {
            column.nextElementSibling.firstElementChild.click()
          } else {
            columns[0].firstElementChild.click();
          }
        }, 0)
      }
    })
  })
}