const src = './src/dependencies';
const components = './src/dependencies/components';

module.exports = {
	components: [
		{
			name: 'title',
			clean: false,
			path: `${components}/title`
		},
		{
			name: 'text',
			clean: true,
			path: `${components}/text`
		},
		{
			name: 'slider',
			clean: true,
			path: `${components}/slider`
		}
	]
}