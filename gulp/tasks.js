const path = './gulp/tasks'
module.exports = [
	`${path}/clean`,
	`${path}/cleanGuide`,
	`${path}/html`,
	`${path}/scripts`,
	`${path}/npmFiles`,
	`${path}/images`,
	`${path}/video`,
	`${path}/audio`,
	`${path}/fonts`,
	`${path}/styles`,
	`${path}/favicons`,
	`${path}/svgSprite`,
	`${path}/cleanDependencies`,
	`${path}/browserSync`
];