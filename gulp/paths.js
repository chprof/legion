const src = './src';
const build = './build';
const dist = './dist';

module.exports = {
	src: {
		html: `${src}/views/**/*.html`,
		fonts: `${src}/assets/fonts/**/*`,
		images: `${src}/assets/img/**/*.{jpg,jpeg,png,gif,svg}`,
		svg: `${src}/assets/icons/svg/*.svg`,
		png: `${src}/assets/icons/png/*.png`,
		audio: `${src}/assets/audio/*`,
		video: `${src}/assets/video/**/*`,
		js: `${src}/core/main.js`,
		scss: `${src}/core/styles.scss`,
		favicons: `${src}/assets/favicons/*.{jpg,jpeg,png,gif}`
	},
	devDest: {
		fonts: `${src}/assets/fonts`,		
	},
	buildDest: {
		html: `${build}`,
		fonts: `${build}/fonts`,
		images: `${build}/img`,
		audio: `${build}/audio`,
		video: `${build}/video`,
		js: `${build}/js`,
		css: `${build}/css`,
	},
	distDest: {
		html: `${dist}`,
		fonts: `${dist}/fonts`,
		images: `${dist}/img`,
		audio: `${dist}/audio`,
		video: `${dist}/video`,
		js: `${dist}/js`,
		css: `${dist}/css`,
	},
	watch: {
		scss: `${src}/**/*.scss`,
		html: `${src}/**/*.html`,
		js: `${src}/**/*.js`,
	},
	filePathForClean: {
		fonts: `${src}/assets/fonts/*.otf`,
		guide: [`${build}/guide.html`]
	}
}