module.exports = html = () => {
	return $.gulp.src($.pathsList.src.html)
		.pipe($.pluginsList['gulp-plumber']({
			errorHandler: $.pluginsList['gulp-notify']
				.onError(err => {
					return {
						title: 'Plumber Error: HTML',
						message: err.message
					}
				})
			})
		)
		.pipe($.pluginsList['gulp-file-include']({
			basepath: '@root'
		}))
		.pipe($.pluginsList['gulp-mode']
			.development(
				$.pluginsList['gulp-cached']()
			)
		)
		.pipe($.pluginsList['gulp-plumber'].stop())
		.pipe($.gulp.dest($.pathsList.buildDest.html))
		.pipe($.pluginsList['gulp-mode']
			.development(
				$.pluginsList['browser-sync'].stream()
			)
		);
};