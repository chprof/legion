module.exports = favicons = () => {
	return $.gulp.src($.pathsList.src.favicons)
		.pipe($.pluginsList['gulp-favicons']({
			icons: {
				appleIcon: false,
				favicons: true,
				online: false,
				appleStartup: false,
				android: false,
				firefox: false,
				yandex: false,
				windows: false,
				coast: false
			}
		}))
		.pipe($.gulp.dest($.pathsList.buildDest.html));
};