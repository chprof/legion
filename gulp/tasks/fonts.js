module.exports = fonts = async (done) => {
  await $.gulp.src(`${$.pathsList.src.fonts}.otf`)
    .pipe($.pluginsList['gulp-fonter']({formats: ['ttf']}))
    .pipe($.gulp.dest($.pathsList.devDest.fonts))

  await $.pluginsList['del']($.pathsList.filePathForClean.fonts);


  $.gulp.src(`${$.pathsList.src.fonts}.ttf`)
    .pipe($.pluginsList['gulp-ttf2woff']())
    .pipe($.gulp.dest($.pathsList.buildDest.fonts))
  
  $.gulp.src(`${$.pathsList.src.fonts}.ttf`)
    .pipe($.pluginsList['gulp-ttf2woff2']())
    .pipe($.gulp.dest($.pathsList.buildDest.fonts))
  
  $.gulp.src(`${$.pathsList.src.fonts}.{woff,woff2}`)
    .pipe($.gulp.dest($.pathsList.buildDest.fonts))

  done()
};