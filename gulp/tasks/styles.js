module.exports = styles = () => {
	return $.gulp.src($.pathsList.src.scss)
		.pipe($.pluginsList['gulp-plumber']({
			errorHandler: $.pluginsList['gulp-notify']
				.onError(err => {
					return {
						title: 'Plumber Error: Styles',
						message: err.message
					}
				})
			})
		)
		.pipe($.pluginsList['gulp-mode']
			.development(
				$.pluginsList['gulp-sourcemaps'].init({
					loadMaps: true
				}),
			)
		)
		.pipe($.pluginsList['gulp-sass']
			({
				outputStyle: 'expanded',
				errLogToConsole: true,
				includePaths: ['node_modules']
			})
			.on('error', $.pluginsList['gulp-sass'].logError)
		)
		.pipe($.pluginsList['gulp-mode']
			.production(
				$.pluginsList['gulp-postcss'](
					[
						$.pluginsList['autoprefixer']({
							cascade: false
						}),
						$.pluginsList['css-mqpacker']({
							sort: $.pluginsList['sort-css-media-queries']
						})
					]
				)
			)
		)
		.pipe($.pluginsList['gulp-mode']
			.development(
				$.pluginsList['gulp-sourcemaps'].write('./'),
			)
		)
		.pipe($.pluginsList['gulp-plumber'].stop())
		.pipe($.gulp.dest($.pathsList.buildDest.css))
		.pipe($.pluginsList['gulp-mode']
			.production(
				$.pluginsList['gulp-rename']('styles.min.css')
			)
		)
		.pipe($.pluginsList['gulp-mode']
			.production(
				$.pluginsList['gulp-clean-css']({
					rebase: false
				})
			)
		)
		.pipe($.pluginsList['gulp-mode']
			.production(
				$.gulp.dest($.pathsList.buildDest.css)
			)
		)
		// .pipe($.gulp.dest($.pathsList.buildDest.css))
		.pipe($.pluginsList['gulp-mode']
			.development(
				$.pluginsList['browser-sync'].stream()
			)
		)
};