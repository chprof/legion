module.exports = cleanDependencies = async (done) => {
	const dependencyCleaned = new Promise((res, rej) => {
		Object.keys($.dependencies).forEach(dependenciesGroup => {
			$.dependencies[dependenciesGroup].forEach(dependency => {
				if(dependency.clean) {
					$.pluginsList['del']([dependency.path])
				}
			})
		})
		res(1)
	})
	dependencyCleaned.then((value) => {
		if(value) done()
	})
};