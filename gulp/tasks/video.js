module.exports = video = (done) => {
  return $.gulp.src($.pathsList.src.video)
    .pipe($.gulp.dest($.pathsList.buildDest.video))
    .on('end', done);
};