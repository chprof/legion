module.exports = scripts = () => {
	return $.gulp.src($.pathsList.src.js)
		.pipe($.pluginsList['gulp-babel']({
				presets: ['@babel/env'],
		}))
		.pipe($.pluginsList['webpack-stream']({
			mode: 'development',
			devtool: 'inline-source-map',
		}))
		.pipe($.pluginsList['gulp-mode']
			.development(
				$.pluginsList['gulp-sourcemaps'].init({
					loadMaps: true
				})
			)
		)
		.pipe($.pluginsList['gulp-rename']('main.js'))
		.pipe($.pluginsList['gulp-mode']
			.development(
				$.pluginsList['gulp-sourcemaps'].write())
			)
		.pipe($.gulp.dest($.pathsList.buildDest.js))
		.pipe($.pluginsList['gulp-mode']
			.production(
				$.pluginsList['gulp-terser']({
					output: { comments: false }
				})
			)
		)
		.pipe($.pluginsList['gulp-mode']
			.production(
				$.pluginsList['gulp-rename']('main.min.js')
			)
		)
		.pipe($.pluginsList['gulp-mode']
			.production(
				$.gulp.dest($.pathsList.buildDest.js)
			)
		)
		// .pipe($.gulp.dest($.pathsList.buildDest.js))
		.pipe($.pluginsList['gulp-mode']
			.development(
				$.pluginsList['browser-sync'].stream()
			)
		);
};