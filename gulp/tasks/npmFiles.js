module.exports = npmFiles = () => {
	return $.gulp.src($.pluginsList['npmfiles']({
			showWarnings: true,
			nodeModulesPath: '../../node_modules',
			packageJsonPath: './package.json',
		}))
		.pipe($.gulp.dest($.pathsList.buildDest.js))
		
};