module.exports = browserSync = (done) => {	
	$.pluginsList['browser-sync'].init({
		server: $.pathsList.buildDest.html,
		// baseDir: $.pathsList.buildDest.html,
		port: 8000,
		notify: false,
		tunnel: 'chdev',
		socket: {
			domain: 'localhost:8000'
		},
		scrollProportionally: false
	});
	$.gulp.watch($.pathsList.watch.html)
		.on('change', $.gulp.series(
			$.tasksList.html,
			$.pluginsList['browser-sync'].reload
		));
	$.gulp.watch($.pathsList.watch.js, scripts);
	$.gulp.watch($.pathsList.watch.scss, styles);
	done()
};