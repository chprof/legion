module.exports = svgSprite = () => {
	return $.gulp.src($.pathsList.src.svg)
		.pipe($.pluginsList['gulp-svgmin']({
			js2svg: {
				pretty: true
			}
		}))
		.pipe($.pluginsList['gulp-cheerio']({
			run: ($) => {
				$('[fill]').removeAttr('fill');
				$('[stroke]').removeAttr('stroke');
				$('[style]').removeAttr('style');
			},
			parserOptions: {xmlMode: true}
		}))
		.pipe($.pluginsList['gulp-replace']('&gt;', '>'))
		.pipe($.pluginsList['gulp-svg-sprite']({
			mode: {
				symbol: {
					dest: $.pathsList.buildDest.images,
					sprite: 'svgSprite.svg',
					example: false,
					render: {
						css: false,
						scss: false
					}
				}
			}
		}))
		.pipe($.gulp.dest('./'))
};