module.exports = images = () => {
	return $.gulp.src($.pathsList.src.images)
		.pipe($.pluginsList['gulp-newer']($.pathsList.buildDest.images))
		.pipe(
			$.pluginsList['gulp-mode'].production(
				$.pluginsList['gulp-imagemin'](
					{
						verbose: true,
						plugins: [
							$.pluginsList['imagemin-jpeg-recompress']({
								loops: 3,
								quality: "high",
								min: 70,
								max: 90,
								progressive: true
							}),
							$.pluginsList['imagemin-svgo']({
								plugins: [
									{
										removeViewBox: false,
										removeUselessStrokeAndFill: false
									}
								]
							}),
							$.pluginsList['imagemin-pngquant']({quality: [0.7, 0.9], speed: 1})
						]
					}
				)
			)
		)
		.pipe($.gulp.dest($.pathsList.buildDest.images))
};