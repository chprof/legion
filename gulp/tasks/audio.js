module.exports = audio = () => {
  return $.gulp.src($.pathsList.src.audio)
    .pipe($.gulp.dest($.pathsList.buildDest.audio));
};