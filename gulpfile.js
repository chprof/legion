global.$ = {
	gulp: require('gulp'),
	pathsObject: require('./gulp/paths.js'),
	variablesObject: require('./gulp/paths.js'),
	pluginsArray: require('./gulp/plugins.js'),
	tasksArray: require("./gulp/tasks.js"),
	dependencies: require("./gulp/dependencies.js"),
	pathsList: {},
	variablesList: {},
	pluginsList: {},
	tasksList: {},
};

$.pluginsArray.forEach(plugin => {
	if(plugin === 'browser-sync') {
		$.pluginsList[plugin] = require(plugin).create();
	} else if(plugin === 'gulp-mode') {
		$.pluginsList[plugin] = require(plugin)();
	} else {
		$.pluginsList[plugin] = require(plugin);
	}
});
$.tasksArray.forEach(taskPath => {
	const taskArr = taskPath.split('/')
	const task = taskArr[taskArr.length - 1]
	$.tasksList[task] = require(taskPath);
});
Object.keys($.pathsObject).forEach(path => {
	$.pathsList[path] = $.pathsObject[path];
});
Object.keys($.variablesObject).forEach(variable => {
	$.variablesList[variable] = $.variablesObject[variable];
});


const test = (done) => {
	// console.log($.dependencies);
	done()
}


exports.clean = $.tasksList.clean;
exports.html = $.tasksList.html;
exports.scripts = $.tasksList.scripts;
exports.npmFiles = $.tasksList.npmFiles;
exports.styles = $.tasksList.styles;
exports.images = $.tasksList.images;
exports.fonts = $.tasksList.fonts;
exports.favicons = $.tasksList.favicons;
exports.svgSprite = $.tasksList.svgSprite;
exports.audio = $.tasksList.audio;
exports.video = $.tasksList.video;
exports.browserSync = $.tasksList.browserSync;
exports.cleanD = $.tasksList.cleanDependencies;
exports.cleanGuide = $.tasksList.cleanGuide;
exports.test = test;

const tasks = 
	$.gulp.series(
		$.tasksList.clean,
		$.gulp.parallel(
			$.tasksList.svgSprite,
			$.tasksList.html,
			$.tasksList.styles,
			$.tasksList.scripts,
			$.tasksList.npmFiles,
			$.tasksList.favicons,
			$.tasksList.images,
			$.tasksList.fonts,
			$.tasksList.audio,
			$.tasksList.video,
		)
	);

exports.build = $.gulp.series(
	tasks
	// $.tasksList.cleanGuide
);
exports.default = $.gulp.series(
	tasks,
	$.tasksList.browserSync
);